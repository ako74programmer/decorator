package com.company;

/*
Concrete Decorators – Extending the base decorator functionality and modifying the
component behavior accordingly. We can have concrete decorator classes
as SwimmingPoolHouse.
 */
public class SwimmingPoolHouse extends HouseDecorator {
    public SwimmingPoolHouse(House house) {
        super(house);
    }

    @Override
    public void build() {
        super.build();
        System.out.print((" with swimming-pool"));
    }
}
