package com.company;

/*
Concrete Decorators – Extending the base decorator functionality and modifying the
component behavior accordingly. We can have concrete decorator classes
as GarageHouse.
 */
public class GarageHouse extends HouseDecorator {
    public GarageHouse(House house) {
        super(house);
    }

    @Override
    public void build() {
        super.build();
        System.out.print((" with garage"));
    }
}
