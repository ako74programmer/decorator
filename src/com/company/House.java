package com.company;

/*
Component Interface – The interface or abstract class defining the methods that will be
implemented. In our case House will be the component interface.
 */
public interface House {
    void build();
}
