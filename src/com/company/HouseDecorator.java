package com.company;

/*
Decorator – Decorator class implements the component interface and it has a HAS-A
relationship with the component interface. The component variable should be accessible to
the child decorator classes, so we will make this variable protected
 */
public class HouseDecorator implements House {
    protected final House house;

    public HouseDecorator(House house) {
        this.house = house;
    }

    @Override
    public void build() {
        house.build();
    }
}
