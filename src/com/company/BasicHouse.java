package com.company;

/*
Component Implementation – The basic implementation of the component interface. We
can have BasicHouse class as our component implementation.
 */
public class BasicHouse implements House {
    @Override
    public void build() {
        System.out.print(("Basic House"));
    }
}
