package com.company;

public class Main {

    public static void main(String[] args) {
        //House - Garage - SwimmingPool - FancyStatues - Garden
        House house = new GardenHouse(new FancyStatuesHouse(new SwimmingPoolHouse(new GarageHouse(new BasicHouse()))));
        house.build();
    }
}
